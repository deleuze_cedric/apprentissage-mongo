const User = require("../model/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

module.exports.register = (req, res) => {

    try {

        // Get user input
        const {email, password} = req.body;

        // Validate user input
        if (!(email && password)) {
            res.status(400).send("All input is required");
        }

        // check if user already exist
        // Validate if user exist in our database
        User.findOne({email}).exec().then((oldUser) => {

            if (oldUser) {
                return res.status(409).send("User Already Exist. Please Login");
            }

            //Encrypt user password
            bcrypt.hash(password, 10).then((encryptedPassword) => {

                // Create user in our database
                User.create({
                    email: email.toLowerCase(), // sanitize: convert email to lowercase
                    password: encryptedPassword,
                }).then((user) => {

                    // Create token
                    user.token = jwt.sign(
                        {user_id: user._id, email},
                        process.env.TOKEN_KEY,
                        {
                            expiresIn: "2h",
                        },
                    );

                    // return new user
                    res.status(201).json(user);

                });

            });

        });

    } catch (err) {
        console.log(err);
    }
}

module.exports.login = async (req, res) => {
    try {
        // Get user input
        const { email, password } = req.body;

        // Validate user input
        if (!(email && password)) {
            res.status(400).send("All input is required");
        }
        // Validate if user exist in our database
        const user = await User.findOne({ email });

        if (user && (await bcrypt.compare(password, user.password))) {
            // Create token
            user.token = jwt.sign(
                { user_id: user._id, email },
                process.env.TOKEN_KEY,
                {
                    expiresIn: "2h",
                }
            );

            // user
            res.status(200).json(user);

        } else {
            res.status(400).send("Invalid Credentials");
        }

    } catch (err) {
        console.log(err);
    }

}

