const Todo = require("../model/todo");
const {ObjectId} = require("mongodb");

module.exports.findAll = async (req, res) => {

    try {
        let tasks = await Todo.find().exec();
        res.json(tasks);
    } catch (err) {
        console.log(err);
    }

}

module.exports.findOne = async (req, res) => {

    try {
        let task = await Todo.findOne({_id: req.params.id}).exec();
        res.json(task);
    } catch (err) {
        console.log(err);
    }
}

module.exports.create = async (req, res) => {

    try {
        let {name} = req.body;

        if (!name) {
            res.status(400).send("All input is required");
        }

        let todo = {
            done: false,
            user: new ObjectId(req.user._id),
            name
        };

        // Create user in our database
        let todoCreated = await Todo.create(todo);

        let task = await Todo.findOne({_id: todoCreated._id}).exec();

        console.log(task);
        // return new user
        res.status(201).json(task);

    } catch (err) {
        console.log(err);
    }

}

module.exports.update = async (req, res) => {

    try {
        let {name, done} = req.body;

        if (!(name && done)) {
            res.status(400).send("All input is required");
        }

        let task = await Todo.findOne({_id: req.params.id}).exec();

        console.log(task);

        if (task) {

            task.done = done ?? task.done;
            task.name = name ?? task.name;

            await task.save();

            res.status(200).json(task);
        } else {
            res.status(404).send("Not found!");
        }
    } catch (err) {
        console.log(err);
    }

}

module.exports.delete = async (req, res) => {

    try {
        await Todo.deleteOne({_id: req.params.id}).exec();
        res.status(204);
        res.send();
    } catch (err) {
        console.log(err);
    }

}