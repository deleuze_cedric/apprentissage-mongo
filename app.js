require("dotenv").config();
require("./config/database").connect();

const todoRoutes = require('./routes/todo');
const userRoutes = require('./routes/user');
const auth = require('./middleware/auth')

const express = require("express");
const app = express();

app.use(express.json());

// Logic goes here

app.use('/todos', auth, todoRoutes)
app.use('/', userRoutes)

module.exports = app;
