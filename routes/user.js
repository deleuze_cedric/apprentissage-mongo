const user = require('../controller/user');
const recordRoutes = require('express').Router();

recordRoutes.route('/register')
    .post(user.register)

recordRoutes.route('/login')
    .post(user.login)

module.exports = recordRoutes;