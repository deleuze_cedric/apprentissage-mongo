const todo = require('../controller/todo');
const recordRoutes = require('express').Router();

recordRoutes.route('/')
    .get(todo.findAll)
    .post(todo.create)

recordRoutes.route('/:id')
    .get(todo.findOne)
    .put(todo.update)
    .delete(todo.delete)

module.exports = recordRoutes;
