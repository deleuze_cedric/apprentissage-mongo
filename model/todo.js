const mongoose = require("mongoose");
const {ObjectId} = require("mongodb");

const taskSchema = new mongoose.Schema({
    name: { type: String, require: true, min: 2 },
    user: { type: ObjectId, required: true },
    done: { type: Boolean, required: true },
});

module.exports = mongoose.model("task", taskSchema)